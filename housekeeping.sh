#!/usr/bin/env bash

echo "Run housekeeping $(date)"

# for f in $(find ./data/ -mindepth 1 -maxdepth 1 -type d -mtime +20); do
#   echo "Removing $f"
#   rm -fr "$f"
# done

while IFS= read -r -d '' file; do
  rm -fr "$file"
done < <(find ./data -mindepth 1 -maxdepth 1 -type d -mtime +20 -print0)
